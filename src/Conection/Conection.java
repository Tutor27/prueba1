package Conection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Conection {
    String url="C:\\Users\\PC\\Documents\\NetBeansProjects\\reto5\\bd\\bdreto5.db";
    public static final String URL = "jdbc:mysql://localhost:3306/bdprueba2";
    public static final String USER = "root";
    public static final String CLAVE = "Stella 172112";
    Connection conn = null;
    
    public Conection() {
        try {
            Class.forName("org.sqlite.JDBC");
            conn = (Connection) DriverManager.getConnection("jdbc:sqlite:"+url);
            System.out.println("Conexión establecida");
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }
    
    public int ejecutarSentenciaSQL(String strSentenciaSQL){
        try {
            PreparedStatement pstm = conn.prepareStatement(strSentenciaSQL);
            pstm.execute();
            System.out.println("Agregado correctamente");
            return 1;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        //Metodo que al ejecutar sentencia se modifica la base de datos
    }
    
    public ResultSet consultarRegistros(String strSentenciaSQL){
        try {
            PreparedStatement pstm = conn.prepareStatement(strSentenciaSQL);
            ResultSet respuesta = pstm.executeQuery();
            return respuesta;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        //Mire si hay información en la tabla y si la hay lo retorna y si no retorna null
    }
}
