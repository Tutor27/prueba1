package Model;

import Model.Producto;

public class ProductoRefrigerado extends Producto {
    public ProductoRefrigerado(String nombre, String id, double temperatura, double valorBase) {
        super(nombre, id, temperatura, valorBase);
    }
    public ProductoRefrigerado() {
    }
    public double calcularCostoDeAlmacenamiento() {
        double costo;
        costo = getValorBase()*1.2;
        return costo;
    }
    @Override public String toString(){
        String mensaje = "ProductoRefrigerado{nombre="+getNombre()+", id="+getId()+", temperatura="+getTemperatura()+", valorBase="+getValorBase()+"}"; 
        return mensaje;
    }
}
