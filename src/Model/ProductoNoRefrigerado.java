package Model;

import Model.Producto;

public class ProductoNoRefrigerado extends Producto {
    public ProductoNoRefrigerado(String nombre, String id, double temperatura, double valorBase) {
        super(nombre, id, temperatura, valorBase);
    }
    public ProductoNoRefrigerado() {
    }
    public double calcularCostoDeAlmacenamiento() {
        double costo;
        costo = getValorBase()*1.1;
        return costo;
    }
    @Override public String toString(){
        String mensaje = "ProductoNoRefrigerado{nombre="+getNombre()+", id="+getId()+", temperatura="+getTemperatura()+", valorBase="+getValorBase()+"}"; 
        return mensaje;
    }
    
}
